import { BaseSchema } from '@adonisjs/lucid/schema';

export default class extends BaseSchema {
   protected tableName = 'user_o_auth_infos';

   async up() {
      this.schema.createTable(this.tableName, (table) => {
         table.increments('id');

         table.string('google_id');
         table.string('github_id');

         table.integer('user_id')
            .notNullable()
            .unsigned()
            .references('id')
            .inTable('users')
            .onDelete('CASCADE');

         table.timestamp('created_at');
         table.timestamp('updated_at');
      });
   }

   async down() {
      this.schema.dropTable(this.tableName);
   }
}
