/*
|--------------------------------------------------------------------------
| Routes file
|--------------------------------------------------------------------------
|
| The routes file is used for defining the HTTP routes.
|
*/

import router from '@adonisjs/core/services/router';
import { middleware } from '#start/kernel';

const UsersController = () => import('#controllers/users_controller');
const AuthController = () => import('#controllers/auth_controller');
const UploadsController = () => import('#controllers/uploads_controller');

router.get('/', async () => {
   return {
      hello: 'world',
   };
});

router.group(() => {
   router.post('/login', [AuthController, 'login']);
});

router.group(() => {
   router.get('/me', [AuthController, 'me']);
   router.resource('/user', UsersController).apiOnly();
}).use(middleware.auth());

router.post('/oauth/google', [AuthController, 'googleSignUp']);

/*
 * File handling and uploading
 */

router.post('/upload', [UploadsController, 'uploadFile']);
router.get('/read', [UploadsController, 'readFile']);
router.post('/write', [UploadsController, 'writeFile']);

router.get('/uploads/*', [UploadsController, 'serveUploadedFile']);
