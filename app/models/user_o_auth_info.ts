import type { DateTime } from 'luxon';
import { BaseModel, belongsTo, column } from '@adonisjs/lucid/orm';
import type { BelongsTo } from '@adonisjs/lucid/types/relations';
import User from '#models/user';

export default class UserOAuthInfo extends BaseModel {
   @column({ isPrimary: true })
   declare id: number;

   @column.dateTime({ autoCreate: true })
   declare createdAt: DateTime;

   @column.dateTime({ autoCreate: true, autoUpdate: true })
   declare updatedAt: DateTime;

   @column()
   declare googleId: string;

   @column()
   declare githubId: string;

   @column()
   declare userId: number;

   @belongsTo(() => User)
   declare user: BelongsTo<typeof User>;
}
