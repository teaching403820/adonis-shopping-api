import vine from '@vinejs/vine';

export const createUserValidator = vine.compile(
   vine.object({
      full_name: vine.string(),
      email: vine.string().email(),
      password: vine.string().minLength(4).confirmed({
         confirmationField: 'confirm_password',
      }),
      role: vine.enum(['SELLER', 'BUYER']),
   }),
);

export const filterUserQueryValidator = vine.compile(
   vine.object({
      q: vine.string().optional(),
      sort_by: vine.string().optional(),
      sort_order: vine.enum(['asc', 'desc']).optional(),
      page: vine.number().optional(),
      limit: vine.number().optional(),
      role: vine.enum(['BUYER', 'SELLER']),
   }),
);
