import vine from '@vinejs/vine';

export const loginUserValidator = vine.compile(
   vine.object({
      email: vine.string().email(),
      password: vine.string(),
   }),
);

export const oAuthGoogleSignUpValidator = vine.compile(
   vine.object({
      email: vine.string().email(),
      fullName: vine.string(),
   }),
);
