import { normalize, sep } from 'node:path';
import { readFileSync, writeFileSync } from 'node:fs';
import type { HttpContext } from '@adonisjs/core/http';
import app from '@adonisjs/core/services/app';
import { cuid } from '@adonisjs/core/helpers';

export default class UploadsController {
   static PATH_TRAVERSAL_REGEX = /(?:^|[\\/])\.\.(?:[\\/]|$)/;

   async uploadFile({ request, response }: HttpContext) {
      //

      const profileImage = request.file('profile_image', {
         size: '1mb',
         extnames: ['jpg'],
      });

      if (!profileImage) {
         return response.badRequest('No image uploaded');
      }

      console.log(profileImage);

      if (profileImage && !profileImage.isValid) {
         return response.badRequest('Illegal file found');
      }

      try {
         const filename = `${cuid()}.${profileImage.extname}`;

         await profileImage.move(app.makePath('uploads/profile'), {
            name: filename,
         });
      }
      catch (e) {
         return response.internalServerError('Failed to upload the image');
      }

      return response.ok('file uploaded');
   }

   /**
    * Serve the uploaded file
    * @param request
    * @param response
    */
   async serveUploadedFile({ request, response }: HttpContext) {
      const filePath = request.param('*').join(sep);
      const normalizedPath = normalize(filePath);

      if (UploadsController.PATH_TRAVERSAL_REGEX.test(normalizedPath)) {
         return response.badRequest('Malformed path');
      }

      console.log(filePath);
      const absolutePath = app.makePath('uploads', normalizedPath);

      return response.download(absolutePath);
   }

   async readFile({ request, response }: HttpContext) {
      try {
         const fileData = readFileSync(`${app.makePath('uploads/data.txt')}`, 'ascii');

         console.log(fileData);

         return response.ok(fileData);
      }
      catch (e) {
         console.log(e);
         return response.internalServerError(e);
      }
   }

   async writeFile({ request, response }: HttpContext) {
      const image = request.file('image', { extnames: ['jpg'] });

      if (image && !image.isValid) {
         return response.badRequest('invalid file format');
      }

      console.log(image);

      if (!image)
         return response.badRequest('no image found');

      try {
         const imageData = readFileSync(image.tmpPath!);

         writeFileSync('uploads/sample.jpg', imageData);
      }
      catch (e) {
         console.log(e);
      }

      return response.ok('ok');
   }
}
