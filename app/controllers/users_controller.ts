import type { HttpContext } from '@adonisjs/core/http';
import { createUserValidator, filterUserQueryValidator } from '#validators/user_validators';
import User from '#models/user';

export default class UsersController {
   /**
    * Get all users
    */
   async index({ response, request }: HttpContext) {
      const query = await filterUserQueryValidator.validate(request.qs());
      query.q = query.q ?? '';
      query.sort_by = query.sort_by ?? 'id';
      query.sort_order = query.sort_order ?? 'asc';
      query.page = query.page ?? 1;
      query.limit = query.limit ?? 2;

      const users = await User
         .query()
         .where('role', query.role)
         .where((dbQuery) => {
            dbQuery
               .whereILike('fullName', `%${query.q}%`)
               .orWhereILike('email', `%${query.q}%`);
         })
         .orderBy(query.sort_by, query.sort_order)
         .forPage(query.page, query.limit);

      return response.ok(users);
   }

   /**
    * Create a new user
    */
   async store({ response, request }: HttpContext) {
      const payload = await request.validateUsing(createUserValidator);

      const existingEmail = await User.findBy({ email: payload.email });

      if (existingEmail)
         return response.badRequest('Email already exists');

      const user = await User.create({
         fullName: payload.full_name,
         email: payload.email,
         role: payload.role,
         password: payload.password,
      });

      return response.ok(user);
   }

   /**
    * Returns a single user
    */
   async show({ response, params }: HttpContext) {
      const user = await User.find(params.id);

      if (!user)
         return response.notFound('User not found');

      return response.ok(user);
   }

   /**
    * Updates a user
    */
   async update({ response }: HttpContext) {
      return response.notImplemented('Not implemented');
   }

   /**
    * Delete a user
    */
   async destroy({ response }: HttpContext) {
      return response.notImplemented('Not implemented');
   }
}
