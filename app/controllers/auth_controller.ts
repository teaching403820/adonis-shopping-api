// import type { HttpContext } from '@adonisjs/core/http'

import type { HttpContext } from '@adonisjs/core/http';
import { cuid } from '@adonisjs/core/helpers';
import { loginUserValidator, oAuthGoogleSignUpValidator } from '#validators/auth_validators';
import User from '#models/user';
import UserOAuthInfo from '#models/user_o_auth_info';

export default class AuthController {
   /**
    * Login controller
    */
   async login({ request, response }: HttpContext) {
      const { email, password } = await request.validateUsing(loginUserValidator);

      const user = await User.verifyCredentials(email, password);

      const token = await this.generateToken(user);

      return response.ok({
         token,
         user,
      });
   }

   /* ------------------------------------------------------------------------------------------- */

   /**
    * Me controller
    */
   async me({ response, auth }: HttpContext) {
      if (!auth.user)
         return response.unauthorized();

      const abilities = auth.user.currentAccessToken.abilities;

      if (!abilities.includes('seller:all')) {
         return response.ok('You are not authorized for selling');
      }

      return response.ok(abilities);
   }

   /* ------------------------------------------------------------------------------------------- */

   /**
    * Google sign up controller
    */
   async googleSignUp({ request, response }: HttpContext) {
      const { email, fullName } = await request.validateUsing(oAuthGoogleSignUpValidator);

      if (!email) {
         return response.badRequest('Need a valid google account email id');
      }

      const oAuthUser = await UserOAuthInfo.findBy('google_id', email);

      if (!oAuthUser) {
         // initiate creating a new user

         const user = await this.createNewUser(email, fullName, 'BUYER');

         const oAuthProfile = await UserOAuthInfo.create({
            userId: user.id,
            googleId: email,
         });

         const token = await this.generateToken(user);

         return response.ok({
            token,
            user,
         });
      }

      await oAuthUser.load('user');

      // fetch the associated user and return
      const token = await this.generateToken(oAuthUser.user);

      return response.ok({
         token,
         user: oAuthUser.user,
      });
   }

   /* ------------------------------------------------------------------------------------------- */

   /**
    * Generates a valid token for the user
    * @private
    */
   private async generateToken(user: User) {
      const existingTokens = await User.accessTokens.all(user);

      existingTokens.forEach((token) => {
         User.accessTokens.delete(user, token.identifier);
      });

      let abilities: string[] = ['*'];

      if (user.role === 'SELLER') {
         abilities = ['seller:all'];
      }
      else {
         abilities = ['buyer:all'];
      }

      return await User.accessTokens.create(user, abilities, { expiresIn: '30 days' });
   }

   /* ------------------------------------------------------------------------------------------- */

   private async createNewUser(email: string, fullName: string, role: 'BUYER' | 'SELLER' = 'BUYER') {
      return await User.create({
         fullName,
         email,
         role,
         password: cuid(),
      });
   }

   /* ------------------------------------------------------------------------------------------- */
}
